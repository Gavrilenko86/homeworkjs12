const buttons = document.querySelectorAll('.btn');
		let activeButton = null;

		function handleButtonClick() {
			const key = this.dataset.key;
			const button = this;

			
			if (button === activeButton) {
				return;
			}

			
			if (activeButton) {
				activeButton.classList.remove('active');
			}

			
			button.classList.add('active');
			activeButton = button;
		}

		
		buttons.forEach(button => {
			button.addEventListener('click', handleButtonClick);
		});

		
		document.addEventListener('keydown', event => {
			const key = event.key.toUpperCase();
			const button = document.querySelector(`.btn[data-key="${key}"]`);

			
			if (!button) {
				return;
			}

			
			button.click();
		});